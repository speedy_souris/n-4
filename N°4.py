#coding:utf-8
"""
EXERCICE PYTHON #4
[Révison : jusqu'aux boucles]

> Développer un mini-terminal tournant en boucle et gérant quelques commandes définies :
    - Votre programme invitera l'utilisateur à saisir une commande (comme la ligne de commande Windows, GNU/Linux, MacOS...)
    - Gérer le cas d'une commande qui n'existe pas
    - Aucun module ne doit être importé

> Quatre commandes à prévoir :
    - run (affiche 5 fois un point avec une pause entre chaque affichage de 1s)
    - name (modifie le nom du terminal, s'appellera 'Défaut' de base)
    - help (affiche la liste des commandes + description brève)
    - quit (termine l'exécution du programme)

> Ce qui sera affiché :
        [non_terminal]>
"""

import time
# Mettre en pause 1s -> time.sleep(1)
