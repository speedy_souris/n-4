#coding:utf-8
"""
EXERCICE PYTHON #4
[Révison : jusqu'aux boucles]

> Développer un mini-terminal tournant en boucle et gérant quelques commandes définies :
    - Votre programme invitera l'utilisateur à saisir une commande (comme la ligne de commande Windows, GNU/Linux, MacOS...)
    - Gérer le cas d'une commande qui n'existe pas
    - Aucun module ne doit être importé

> Quatre commandes à prévoir :
    - run (affiche 5 fois un point avec une pause entre chaque affichage de 1s)
    - name (modifie le nom du terminal, s'appellera 'Défaut' de base)
    - help (affiche la liste des commandes + description brève)
    - quit (termine l'exécution du programme)

> Ce qui sera affiché :
        [non_terminal]>
"""

import time
# Mettre en pause 1s -> time.sleep(1)

noName = True
terminal = "Défaut"
temps, tmp = 5, 1

while noName:
    print()
    choix = input(f"[{terminal}]> ")
#                         -----------------------
    # Commande run
    if choix == "run":
        while tmp <= temps:
            print(".")
            time.sleep(1)
            tmp += 1
#                         -----------------------
    # Commande name
    elif choix == "name":
        noletter = True
        letters = ""
        while noletter:
            termName = input("\nDonnez un nouveau nom de terminal (Max 12 lettres) : ")

            for letter in termName:
                if ord(letter.lower()) in range(97, 123) and len(termName) <= 12:
                    letters += letter
                else:
                    print("\nle nom doit être composer que de 12 lettres maxi")
                    break
                noletter = False

        terminal = letters
#                         -----------------------
    elif choix == "help":
        print("""\nCe mini_terminal dispose de 4 commandes :
        - <run> qui permet d'afficher 5 <.> avec un delais d'1 seconde entre chaque affichage
        - <name> qui permet de changer le nom du terminal avec 12 lettres maxi
        - <help> donne une description de chaque commande
        - <quit> permet de quitter le programme...""")
#                         -----------------------
    elif choix == "quit":
        print()
        qt = input("Voulez vous vraiment quitter Oui/Non : ")
        if qt.lower() == "o":
            print("\nAu revoir !")
            print("A bientôt\n")
            noName = False
        elif qt.lower() == "n":
            continue
    else:
        print("\nCeci n'est pas une commande reconnu")
        continue
